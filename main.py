from gtts import gTTS
import speech_recognition as speak
import os
import re
import webbrowser
import smtplib
import requests
from weather import Weather, Unit
#import datetime
from time import gmtime, strftime


#Вимова
def talkToMe(audio):
    print(audio)
    
#    for line in audio.splitlines():
#        os.system('say ' + audio)
    
    tts = gTTS(text=audio, lang='en')
    tts.save('audio.mp3')
    os.system('mpg123 audio.mp3')

#Сприйняття команд    
def myCommand():
    r = speak.Recognizer()
    
    with speak.Microphone() as source:
        print('Listening...')
        
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)
    
    try:
        command = r.recognize_google(audio).lower()
        print('Command:',command)
    
    except speak.UnknownValueError:
        print('Listening error')
        command = myCommand()
    
    return command


#Список команд
def assistant(command):
    
#    Відкрити лінк
    if 'open website' in command:
        reg_ex = re.search('open website (.+)', command)
        if reg_ex:
            domain = reg_ex.group(1)
            url = 'https://www.' + domain
            webbrowser.open(url)
            print('Done!')
        else:
            pass
    
#    elif 'open google' or 'google' in command:
#        url='https://www.google.com'
#        webbrowser.open(url)
#    
#    elif 'open youtube' or 'youtube' in command:
#        url='https://www.youtube.com'
#        webbrowser.open(url)
    
#    Час
    elif 'time' in command:
        hours = str(int(strftime("%H", gmtime()).lstrip('0'))+3)
#        Беремо 3 часовий пояс
        minutes = strftime("%M", gmtime()).lstrip('0')
        seconds = strftime("%S", gmtime()).lstrip('0')
        time = 'It is {0} hours, {1} minutes and {2} seconds'.format(hours, minutes, seconds)
        talkToMe(time)
        
#    Дата
    elif 'date' in command:
        pass
    
#    Ізюм
    elif 'whatsapp' in command:
        os.system('mpg123 wassup.mp3')
    
#    Англійська цукерочка
    elif 'joke' in command:
        res = requests.get(
                'https://icanhazdadjoke.com/',
                headers={"Accept":"application/json"}
                )
        if res.status_code == requests.codes.ok:
            talkToMe(str(res.json()['joke']))
        else:
            talkToMe('oops!I ran out of jokes')
            
#    Показати теперішню погоду
    elif 'weather in' in command:
        reg_ex = re.search('weather in (.*)', command)
        if reg_ex:
            city = reg_ex.group(1)
            api_address = 'http://api.openweathermap.org/data/2.5/weather?appid=0c42f7f6b53b244c78a418f4f181282a&q='
            units = '&units=metric'
            url = api_address + city + units
            json_data = requests.get(url).json()
            
            weather_data = json_data['weather'][0]['description']
            temp = json_data['main']['temp']
            pressure = json_data['main']['pressure']
            humidity = json_data['main']['humidity']
            
            talkToMe('It is {0} with {1} degrees. Pressure is {2} millimeters of mercury column. Humidity is {3}%'.format(weather_data, temp, pressure, humidity))
            

talkToMe('Here and on')

while True:
    assistant(myCommand())